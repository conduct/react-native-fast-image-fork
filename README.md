# React Native Fast Image

This is a fork of version `5.1.2` of the [`react-native-fast-image`](https://github.com/DylanVann/react-native-fast-image) package.

## Differences

1. Introduces a `resizeImageAndroid` prop. [See pull request here.](https://github.com/DylanVann/react-native-fast-image/pull/401)
2. Fixes a potential iOS memory leak when using `onLoad`. [See pull request here.](https://github.com/DylanVann/react-native-fast-image/pull/398)

At the time of writing, neither of these pull requests have been merged into the master for a release on NPM. If they have since been merged, you are likely safe to upgrade back to the regular package after carefully checking the official [release changelog](https://github.com/DylanVann/react-native-fast-image/releases) for any major changes post version `5.1.2`.